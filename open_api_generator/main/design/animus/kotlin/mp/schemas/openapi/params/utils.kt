package design.animus.kotlin.mp.schemas.openapi.params

import com.squareup.kotlinpoet.TypeName
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.toClassName
import design.animus.kotlin.mp.schemas.openapi.generated.OpenAPI
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Parameter
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.ParameterSerializer
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfComponentsParameters
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfParameterSchema
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfParameters
import design.animus.kotlin.mp.schemas.openapi.logger
import design.animus.kotlin.mp.schemas.openapi.utils.toTypeName
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@ExperimentalSerializationApi
suspend fun List<OneOfParameters>.reconcile(schema: OpenAPI): List<@Serializable(with = ParameterSerializer::class) Parameter> {
  val out = this.map { possible ->
    when (possible) {
      is OneOfParameters.PossibleParameter -> possible.item
      is OneOfParameters.PossibleReference -> {
        val ref = possible.item.schemaPatternProperties.map["\$ref"] ?: error("Found an empty reference")
        logger.debug { "Found reference of: $ref" }
        val refName = ref.split("/").last()
        val param =
          schema.components?.parameters?.schemaPatternProperties?.map?.get(refName) as? OneOfComponentsParameters.PossibleParameter
            ?: error("Reference was another reference this should not occur")
        param.item
      }
    }
  }
  return out
}

@ExperimentalSerializationApi
suspend fun Parameter.toTypeName(config: GeneratorConfig): TypeName {
  return when (this.schema) {
    is OneOfParameterSchema.PossibleSchema -> (this.schema as OneOfParameterSchema.PossibleSchema).item.toTypeName(
      this.name,
      config
    )
    is OneOfParameterSchema.PossibleReference -> (this.schema as OneOfParameterSchema.PossibleReference).item.toTypeName(
      config
    )
    null -> JsonElement::class.toClassName()
  }
}
