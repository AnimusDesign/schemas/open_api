package design.animus.kotlin.mp.schemas.openapi

import kotlinx.coroutines.runBlocking
import kotlinx.serialization.ExperimentalSerializationApi
import mu.KotlinLogging
import org.gradle.api.Plugin
import org.gradle.api.Project

internal val logger = KotlinLogging.logger { }

@ExperimentalSerializationApi
class OpenAPIGenerator : Plugin<Project> {
  override fun apply(project: Project) {
    val extension = project.extensions.create<OpenAPIGeneratorConfig>(
      "OpenAPIGeneratorConfig",
      OpenAPIGeneratorConfig::class.java
    )
    project.task("openAPIGenerate")
      .apply {
        group = "open_api"
      }.doFirst { _ ->
        runBlocking {
          extension.schemas.forEach { schema ->
            OpenAPIGeneratorExecutor(this).generate(schema)
          }
        }
      }
  }
}
