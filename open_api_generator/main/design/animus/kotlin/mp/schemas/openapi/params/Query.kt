package design.animus.kotlin.mp.schemas.openapi.params

import com.squareup.kotlinpoet.CodeBlock
import com.squareup.kotlinpoet.ParameterSpec
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Parameter
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.ParameterSerializer
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable

data class QueryResponse(
  val requiredCodeBlock: List<CodeBlock>,
  val requiredParameters: List<ParameterSpec>,
  val optionalCodeBlock: List<CodeBlock>,
  val optionalParams: List<ParameterSpec>
)

@ExperimentalSerializationApi
suspend fun handleQueryParams(
  params: List<@Serializable(with = ParameterSerializer::class) Parameter>,
  config: GeneratorConfig
): QueryResponse {
  val grouping = params
    .filter { it.`in`.equals("query", ignoreCase = true) }
    .groupBy { it.required ?: false }
  val required = grouping[true] ?: listOf()
  val optional = grouping[false] ?: listOf()
  val requiredArgs = required.map { rParam ->
    val param = ParameterSpec.builder(rParam.name, rParam.toTypeName(config))
      .addKdoc(rParam.description ?: "")
    Pair(
      param.build(),
      CodeBlock.Builder()
        .addStatement("parameter(%S, %L)", rParam.name, rParam.name)
        .build()
    )
  }
  val optionalArgs = optional.map { oParam ->
    val param = ParameterSpec.builder(oParam.name, oParam.toTypeName(config).copy(nullable = true))
      .addKdoc(oParam.description ?: "")
      .defaultValue("%L", null)
    val optionalArgCode = CodeBlock.Builder()
      .beginControlFlow("if (${oParam.name} != null)")
      .addStatement("parameter(%S, %L)", oParam.name, oParam.name)
      .endControlFlow()
      .build()
    Pair(param.build(), optionalArgCode)
  }
  return QueryResponse(
    requiredCodeBlock = requiredArgs.map { it.second },
    requiredParameters = requiredArgs.map { it.first },
    optionalCodeBlock = optionalArgs.map { it.second },
    optionalParams = optionalArgs.map { it.first }
  )
}
