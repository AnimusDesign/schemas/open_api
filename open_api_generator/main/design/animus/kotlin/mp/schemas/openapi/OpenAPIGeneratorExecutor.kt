package design.animus.kotlin.mp.schemas.openapi

import design.animus.functional.datatypes.either.Either
import design.animus.functional.datatypes.either.Error
import design.animus.functional.datatypes.either.catchEitherAsync
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.JSONSchemaGeneratorExecutor
import design.animus.kotlin.mp.schemas.json.parser.JSONSchemaParser
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchema
import design.animus.kotlin.mp.schemas.json.parser.schema.JSONSchemaObject
import design.animus.kotlin.mp.schemas.openapi.client.createAndWriteParent
import design.animus.kotlin.mp.schemas.openapi.generated.OpenAPI
import design.animus.kotlin.mp.schemas.openapi.generated.OpenAPISerializer
import design.animus.kotlin.mp.schemas.openapi.generated.patternmaps.PathsWrapperMap
import design.animus.kotlin.mp.schemas.openapi.operationfunc.createOperationFuncFromPathItemAndWriteToFile
import design.animus.kotlin.mp.schemas.openapi.server.createServerAsync
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerializationException
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive

@ExperimentalSerializationApi
internal lateinit var openAPI: OpenAPI
internal lateinit var jsonSchemaObject: JSONSchemaObject

@ExperimentalSerializationApi
class OpenAPIGeneratorExecutor(private val ctx: CoroutineScope) {

  suspend fun parseToJsonSchema(config: GeneratorConfig): JSONSchema {
    val schemaObj = Json.decodeFromString<JsonObject>(config.schemaFile.readText())
    val components = schemaObj["components"] as? JsonObject
      ?: throw SerializationException("Could not cast components to object")
    val schemas = components["schemas"] as? JsonObject
      ?: throw SerializationException("Could not cast schema to json object,")
    val asJsonSchema = JsonObject(
      mapOf(
        "\$schema" to JsonPrimitive("http://json-schema.org/draft-04/schema#"),
        "type" to JsonPrimitive("object"),
        "definitions" to schemas
      )
    )
    return JSONSchemaParser.parseJSON(Json.encodeToString(asJsonSchema))
  }

  suspend fun generateClient(config: GeneratorConfig) {
    val clientConfig = config.copy(packageBase = "${config.packageBase}.client")
    val schema = Json {
      ignoreUnknownKeys = true
    }.decodeFromString(OpenAPI.serializer(), clientConfig.schemaFile.readText())
    openAPI = schema
    val basePackageName = "${clientConfig.packageBase}.client"
    val parentClass = createAndWriteParent(clientConfig)
    openAPI.paths.schemaPatternProperties.forEach { pattern ->
      when (pattern) {
        is PathsWrapperMap.Paths0Map -> {
          pattern.map.forEach { (path, pathDef) ->
            createOperationFuncFromPathItemAndWriteToFile(path, pathDef, basePackageName, clientConfig)
          }
        }
        is PathsWrapperMap.Paths1Map -> TODO()
      }
    }
  }

  private suspend fun getSchemaAsync(config: GeneratorConfig): Deferred<Either<Error, OpenAPI>> =
    ctx.catchEitherAsync {
      openAPI = Json.decodeFromString(OpenAPISerializer, config.schemaFile.readText())
      openAPI
    }

  suspend fun generateServer(config: GeneratorConfig) = Either.comprehensionAsync<Exception, Unit> {
    val (schema) = getSchemaAsync(config)
    val paths = schema.paths.schemaPatternProperties.filterIsInstance<PathsWrapperMap.Paths0Map>().firstOrNull()
      ?: error("Expected at least one path item")
    paths.map.map { (path, definition) ->
      val rsp = createServerAsync(path, definition, config, ctx).await()
      when (rsp) {
        is Either.Left -> logger.error { "Failed to create server path:$path - ${rsp.left}" }
        is Either.Right -> logger.debug { "Created server path: $path" }
      }
    }
  }

  suspend fun createModel(config: OpenAPIConfig) {
    val raw = parseToJsonSchema(config.generatorConfig)
    jsonSchemaObject = raw as JSONSchemaObject
    JSONSchemaGeneratorExecutor.processJSONSchemaObject(
      raw as JSONSchemaObject,
      config.generatorConfig,
      config.generatorConfig.createBaseObject
    )
  }

  suspend fun generate(config: OpenAPIConfig) {
    if (config.createModel) {
      logger.debug { "Model generation is enabled." }
      createModel(config)
    }
    if (config.createServer) {
      logger.debug { "Server generation is enabled" }
      generateServer(config.generatorConfig)
    }
    if (config.createClient) {
      logger.debug { "Client generation is enabled." }
      generateClient(config.generatorConfig)
    }
  }
}
