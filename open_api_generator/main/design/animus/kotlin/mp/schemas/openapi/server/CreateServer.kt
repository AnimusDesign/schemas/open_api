package design.animus.kotlin.mp.schemas.openapi.server

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.PropertySpec
import com.squareup.kotlinpoet.TypeSpec
import design.animus.functional.datatypes.either.Either
import design.animus.functional.datatypes.either.catchEitherAsync
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.fileSpecBootStrap
import design.animus.kotlin.mp.schemas.json.generator.sanitizeForClassName
import design.animus.kotlin.mp.schemas.json.generator.toClassName
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Operation
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.PathItem
import design.animus.kotlin.mp.schemas.openapi.generated.patternmaps.PathItemWrapperMap
import design.animus.kotlin.mp.schemas.openapi.logger
import design.animus.kotlin.mp.schemas.openapi.openAPI
import design.animus.kotlin.mp.schemas.openapi.params.reconcile
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.awaitAll

suspend fun createServerAsync(path: String, definition: PathItem, config: GeneratorConfig, ctx: CoroutineScope) =
  ctx.catchEitherAsync {
    logger.debug { "Configuring path way: $path" }
    val operationDefinitions =
      definition.schemaPatternProperties.filterIsInstance<PathItemWrapperMap.PathItem0Map>().firstOrNull()
        ?: error("Expected operations to not be null")
    val operations = operationDefinitions.map.map { (verb, operation) ->
      createOperationAsync(path, verb, operation, config, ctx)
    }
    operations.awaitAll()
  }

suspend fun createOperationAsync(
  path: String,
  verb: String,
  operation: Operation,
  config: GeneratorConfig,
  ctx: CoroutineScope
) =
  ctx.catchEitherAsync {
    val packageName = "${config.packageBase}.server"
    val operationName = operation.operationId?.sanitizeForClassName()?.className ?: "Banana"
    val file = fileSpecBootStrap(packageName, operationName)
    val params = operation.parameters?.reconcile(openAPI) ?: listOf()
    val pathParams = params.filter { it.`in`.toLowerCase() == "path" }.sortedBy { it.required ?: false }
    val obj = TypeSpec.objectBuilder(operationName)
      .addSuperinterface(IOpenAPI::class)
      .addKdoc(operation.description ?: "")
      .addProperty(
        PropertySpec.builder("operationName", String::class)
          .addModifiers(KModifier.OVERRIDE)
          .initializer("%S", operation.operationId ?: "")
          .build()
      )
      .addProperty(
        PropertySpec.builder("path", OpenAPIPath::class)
          .addModifiers(KModifier.OVERRIDE)
          .initializer("%S", path)
          .build()
      )
      .addProperty(
        PropertySpec.builder("verb", HTTPVerb::class)
          .addModifiers(KModifier.OVERRIDE)
          .initializer("%T", HTTPVerb.fromString(verb)::class)
          .build()
      )
    val err = Either.comprehensionAsync<Exception, Unit> {
      if (pathParams.isNotEmpty()) {
        val (pathParamTypes) = !createPathParameterAsync(
          pathParams,
          operationName,
          packageName,
          config,
          ctx
        )
        val (dataClass, pathParamProp, createPathParamFunc) = pathParamTypes
        val dataClassName = "${operationName}PathParams"
        val dataClassType = ClassName(packageName, dataClassName)
        obj
          .addSuperinterface(
            IOpenAPIWithPathParameters::class.toClassName().parameterizedBy(dataClassType)
          )
          .addProperty(pathParamProp)
          .addFunction(createPathParamFunc)
        file.addType(dataClass)
      }

      file.addType(obj.build())
      file.build().writeTo(config.outputPath)
      true
    }
    when (err) {
      is Either.Left -> throw err.left
      is Either.Right -> err.right
    }
  }
