package design.animus.kotlin.mp.schemas.openapi.utils

import design.animus.kotlin.mp.schemas.json.generator.*
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Schema
import design.animus.kotlin.mp.schemas.openapi.generated.enums.EnumSchemaType
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfMediaTypeSchema
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfParameterSchema
import kotlin.reflect.KClass
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.JsonObject

@ExperimentalSerializationApi
/**
 * TODO Clean up handling of nested types
 * TODO clean up enums kclass
 */
suspend fun Schema.toType(name: String, config: GeneratorConfig): KClass<out Any> {
  val rsp = if (this.enum != null && this.enum!!.isNotEmpty()) {
    Any::class
  } else {
    when (this.type) {
      EnumSchemaType.array -> List::class
      EnumSchemaType.boolean -> Boolean::class
      EnumSchemaType.integer -> Int::class
      EnumSchemaType.number -> Number::class
      EnumSchemaType.`object` -> JsonObject::class
      EnumSchemaType.string -> String::class
      null -> String::class
    }
  }
  return rsp
}

@ExperimentalSerializationApi
suspend fun OneOfParameterSchema.toType(name: String, config: GeneratorConfig): KClass<*> {
  return when (this) {
    is OneOfParameterSchema.PossibleSchema -> this.item.toType(name, config)
    is OneOfParameterSchema.PossibleReference -> this.item.toType(config)
  }
}

@ExperimentalSerializationApi
suspend fun OneOfMediaTypeSchema.toType(name: String, config: GeneratorConfig): KClass<*> {
  return when (this) {
    is OneOfMediaTypeSchema.PossibleSchema -> this.item.toType(name, config)
    is OneOfMediaTypeSchema.PossibleReference -> this.item.toType(config)
  }
}
