package design.animus.kotlin.mp.schemas.openapi.utils

import com.squareup.kotlinpoet.TypeName
import design.animus.functional.datatypes.option.Option
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.parser.schema.types.PropertyTypes
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Schema
import design.animus.kotlin.mp.schemas.openapi.logger
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.*

suspend fun List<JsonElement>.toEnumProperty(): PropertyTypes.Containers.EnumProperty {
  val castValues = this.mapNotNull {
    when (it) {
      is JsonPrimitive -> {
        val prim = it as JsonPrimitive
        prim.content
      }
      JsonNull,
      is JsonObject,
      is JsonArray -> {
        logger.warn { "Json null, object, and array are not supported as an enum value received: $it" }
        null
      }
    }
  }
  return PropertyTypes.Containers.EnumProperty(castValues)
}

@ExperimentalSerializationApi
suspend fun Schema.toEnumProperty(): PropertyTypes.Containers.EnumProperty {
  val values = this.enum ?: listOf()
  return values.toEnumProperty()
}

@ExperimentalSerializationApi
suspend fun Schema.createEnum(name: String, config: GeneratorConfig): TypeName {
  return design.animus.kotlin.mp.schemas.json.generator.containers.createEnum(
    Option.None(),
    name,
    this.toEnumProperty(),
    config,
    isDefinition = false
  )
}
