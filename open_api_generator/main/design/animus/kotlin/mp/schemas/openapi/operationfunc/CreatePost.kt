package design.animus.kotlin.mp.schemas.openapi.operationfunc

import com.squareup.kotlinpoet.FunSpec
import design.animus.kotlin.mp.schemas.openapi.logger
import design.animus.kotlin.mp.schemas.openapi.utils.containsForm
import design.animus.kotlin.mp.schemas.openapi.utils.hasMultipleBodies
import design.animus.kotlin.mp.schemas.openapi.utils.toRequestBody
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
suspend fun createFormPost(args: OperationFuncArgs): FunSpec {
  TODO()
}

@ExperimentalSerializationApi
suspend fun createBodyPost(args: OperationFuncArgs): FunSpec {
  val allParams = args.urlParams.params + args.queryParams.requiredParameters + args.queryParams.optionalParams
  val operationFunc = bootStrapOperationFunc(args)
    .addCommonClientInitStatement("get", args.urlParams.apiURL)
    .addParameters(allParams)

  return (operationFunc.build())
}

@ExperimentalSerializationApi
suspend fun createMultiBodyPost(args: OperationFuncArgs): FunSpec {
  TODO()
}

@ExperimentalSerializationApi
suspend fun createPost(args: OperationFuncArgs) {
  if (args.operation.requestBody != null) {
    logger.debug { "Request Body is not null" }
    val requestBody = args.operation.requestBody!!.toRequestBody()
    when {
      requestBody.containsForm() -> {
        createFormPost(args)
      }
      requestBody.hasMultipleBodies(args.config) -> {
        createMultiBodyPost(args)
      }
      else -> {
        createBodyPost(args)
      }
    }
  }
  //            if (operation.requestBody != null) {
}
