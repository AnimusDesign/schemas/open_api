package design.animus.kotlin.mp.schemas.openapi.records

enum class HTTPVerbs {
  GET, POST, PUT, DELETE;
}

suspend fun getHTTPVerb(inVerb: String) = HTTPVerbs.values().firstOrNull { verb ->
  verb.name.equals(inVerb, ignoreCase = true)
} ?: error("Could not find verb that matches in verb of: $inVerb")
