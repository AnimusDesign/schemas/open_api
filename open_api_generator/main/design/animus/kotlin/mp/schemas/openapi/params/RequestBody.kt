package design.animus.kotlin.mp.schemas.openapi.params

// @ExperimentalSerializationApi
// suspend fun requestBodyToParamAndCode(requestBody: OneOfOperationRequestBody) {
//    val requestParam = when (requestBody) {
//        is OneOfOperationRequestBody.PossibleRequestBody -> {
//            val castRequestBody = requestBody
//            val expectedValues = castRequestBody.item.content.schemaAdditionalProperties.values.mapNotNull {
//                it.schema?.toTypeName(config)
//            }
//                .toSet()
//            if (expectedValues.size > 1 || expectedValues.isEmpty()) error("Expecting a single content type for body upload: ${castRequestBody.item.content}.")
//            val jsonKey = castRequestBody.item.content.schemaAdditionalProperties.keys.firstOrNull()
//                ?: error("Operation at path:${EnumParameterInPathAnonymousParameterInPathParameterLocationIn.path} for verb:$verb does not contain a json request body type")
//            val requestBody = castRequestBody.item.content.schemaAdditionalProperties[jsonKey]!!
//            val requestBodyType = requestBody.schema?.toTypeName(config)!!
//            ParameterSpec.builder("inBody", requestBodyType)
//                .addKdoc(castRequestBody.item.description ?: "")
//                .build()
//        }
//        is OneOfOperationRequestBody.PossibleReference -> {
//            val castRequestBody = operation.requestBody as OneOfOperationRequestBody.PossibleReference
//            val requestType = castRequestBody.item.toTypeName(config)
//            ParameterSpec.builder("inBody", requestType).build()
//        }
//    }
// }
