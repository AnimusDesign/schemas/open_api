package design.animus.kotlin.mp.schemas.openapi.operationfunc.common

import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.TypeSpec
import design.animus.kotlin.mp.schemas.json.generator.toClassName
import design.animus.kotlin.mp.schemas.openapi.responses.Blank
import design.animus.kotlin.mp.schemas.openapi.responses.PossibleResponses

suspend fun FunSpec.Builder.addResponses(possibleResponse: PossibleResponses): List<TypeSpec> {
  return when (possibleResponse) {
    PossibleResponses.Blank -> {
      this.addStatement("}")
        .addStatement("return %T", Blank::class)
        .returns(Blank::class.toClassName())
      listOf()
    }
    is PossibleResponses.SerializableResponse -> {
      val (responseType, responseSerializer, responseRef) = possibleResponse
      this.returns(responseRef)
        .addStatement("}")
        .addStatement("val result = Json.decodeFromString(${responseSerializer.name}, rsp)".trim())
        .addStatement("return result".trim())
      listOf(responseSerializer, responseType)
    }
  }
}
