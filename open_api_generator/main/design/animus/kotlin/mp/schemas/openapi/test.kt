package design.animus.kotlin.mp.schemas.openapi

import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import java.io.File
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
suspend fun main() {
  runBlocking {
    val home = System.getenv("HOME") ?: error("Home variable was not set.")
    val schemaPath = "$home/dev/schemas/open_api/tests/petstore/src/test/resources/petstore.api.json"
    val destPath = "$home/dev/schemas/open_api/tests/petstore/generated"
    val config = GeneratorConfig(
      packageBase = "design.animus.kotlin.mp.schemas.openapi.tests.petstore",
      outputPath = File(destPath),
      schemaFile = File(schemaPath),
      schemaName = "PetStore"
    )
    val openAPIGenerator = OpenAPIGeneratorExecutor(this)
    openAPIGenerator.generateServer(config)
  }
}
