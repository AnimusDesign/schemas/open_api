package design.animus.kotlin.mp.schemas.openapi.params

import com.squareup.kotlinpoet.ParameterSpec
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.openapi.generated.OpenAPI
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Reference
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfComponentsParameters
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
suspend fun createFunctionParameterForReference(
  param: Reference,
  schema: OpenAPI,
  config: GeneratorConfig
): ParameterSpec {
  val refName = param.schemaPatternProperties.map["\$ref"] ?: error("Could not find reference key.")
  val paramSchema = schema.components?.parameters?.schemaPatternProperties?.map?.get(refName)
    ?: error("Could not find schema based on reference.")
  return when (paramSchema) {
    is OneOfComponentsParameters.PossibleReference -> createFunctionParameterForReference(paramSchema.item, schema, config)
    is OneOfComponentsParameters.PossibleParameter -> createFunctionParameterForSchema(paramSchema.item, config)
  }
}
