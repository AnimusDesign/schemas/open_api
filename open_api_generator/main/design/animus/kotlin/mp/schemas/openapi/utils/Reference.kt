package design.animus.kotlin.mp.schemas.openapi.utils

import com.squareup.kotlinpoet.ClassName
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.mutateNameIfEnabled
import design.animus.kotlin.mp.schemas.json.generator.mutatePackageNameIfEnabled
import design.animus.kotlin.mp.schemas.openapi.generated.OpenAPI
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Reference
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfComponentsParameters
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfComponentsSchemas
import design.animus.kotlin.mp.schemas.openapi.openAPI
import kotlin.reflect.KClass
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
suspend fun Reference.toTypeName(config: GeneratorConfig): ClassName {
  val refPath = this.schemaPatternProperties.map["\$ref"] ?: error("Could not find \$ref in map.")
  val itemName = refPath.split("/").last()
  val packageName = mutatePackageNameIfEnabled("${config.packageBase}.definitions", itemName, config)
  val (target, _, _) = mutateNameIfEnabled(itemName, config)
  return ClassName(packageName, target)
}

@ExperimentalSerializationApi
suspend fun Reference.toType(config: GeneratorConfig): KClass<*> {
  val refPath = this.schemaPatternProperties.map["\$ref"] ?: error("Could not find \$ref in map.")
  val itemName = refPath.split("/").last()
  val packageName = mutatePackageNameIfEnabled("${config.packageBase}.definitions", itemName, config)
  val (target, _, _) = mutateNameIfEnabled(itemName, config)
  val schema = openAPI.components?.schemas?.schemaPatternProperties?.map?.map { (name, definition) ->
    if (name === itemName) definition else null
  }?.filterNotNull()?.firstOrNull() ?: error("Could not find referenced schema")
  return when (schema) {
    is OneOfComponentsSchemas.PossibleReference -> schema.item.toType(config)
    is OneOfComponentsSchemas.PossibleSchema -> schema.item.toType(itemName, config)
  }
}

@ExperimentalSerializationApi
suspend fun Reference.toSchema(schema: OpenAPI): OneOfComponentsParameters {
  val refName = this.schemaPatternProperties.map["\$ref"] ?: error("Could not find reference in schema.")
  return schema.components?.parameters?.schemaPatternProperties?.map?.get(refName)
    ?: error("Could not find schema based on reference.")
}
