package design.animus.kotlin.mp.schemas.openapi.params

import com.squareup.kotlinpoet.ParameterSpec
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.openapi.generated.OpenAPI
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Reference
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfComponentsParameters
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfParameters
import design.animus.kotlin.mp.schemas.openapi.utils.toSchema
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
suspend fun isReferenceParamRequired(ref: Reference, schema: OpenAPI, config: GeneratorConfig): Boolean {
  return when (val paramSchema = ref.toSchema(schema)) {
    is OneOfComponentsParameters.PossibleReference -> isReferenceParamRequired(paramSchema.item, schema, config)
    is OneOfComponentsParameters.PossibleParameter -> paramSchema.item.required ?: false
  }
}

@ExperimentalSerializationApi
suspend fun List<OneOfParameters>.findByName(name: String, schema: OpenAPI, config: GeneratorConfig): Pair<OneOfParameters, Boolean> {
  val param = this.first { param ->
    when (param) {
      is OneOfParameters.PossibleParameter -> {
        param.item.name == name
      }
      is OneOfParameters.PossibleReference -> {
        val refName = param.item.schemaPatternProperties.map["\$ref"] ?: error("Could not find reference key.")
        schema.components?.parameters?.schemaPatternProperties?.map?.keys?.contains(refName) ?: false
      }
      else -> false
    }
  }
  val required = when (param) {
    is OneOfParameters.PossibleParameter -> param.item.required ?: false
    is OneOfParameters.PossibleReference -> isReferenceParamRequired(param.item, schema, config)
  }
  return Pair(param, required)
}

@ExperimentalSerializationApi
suspend fun createFunctionParameterFromParameter(param: OneOfParameters, schema: OpenAPI, config: GeneratorConfig): ParameterSpec {
  return when (param) {
    is OneOfParameters.PossibleParameter -> createFunctionParameterForSchema(param.item, config)
    is OneOfParameters.PossibleReference -> createFunctionParameterForReference(param.item, schema, config)
  }
}
