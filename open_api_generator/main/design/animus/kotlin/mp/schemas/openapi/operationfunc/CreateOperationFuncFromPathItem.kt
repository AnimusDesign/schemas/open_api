package design.animus.kotlin.mp.schemas.openapi.operationfunc

import com.squareup.kotlinpoet.*
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.fileSpecBootStrap
import design.animus.kotlin.mp.schemas.json.generator.sanitizeForClassName
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Operation
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Parameter
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.PathItem
import design.animus.kotlin.mp.schemas.openapi.generated.patternmaps.PathItemWrapperMap
import design.animus.kotlin.mp.schemas.openapi.logger
import design.animus.kotlin.mp.schemas.openapi.openAPI
import design.animus.kotlin.mp.schemas.openapi.params.QueryResponse
import design.animus.kotlin.mp.schemas.openapi.params.handleQueryParams
import design.animus.kotlin.mp.schemas.openapi.params.reconcile
import design.animus.kotlin.mp.schemas.openapi.records.HTTPVerbs
import design.animus.kotlin.mp.schemas.openapi.records.getHTTPVerb
import design.animus.kotlin.mp.schemas.openapi.responses.PossibleResponses
import design.animus.kotlin.mp.schemas.openapi.responses.determineReturnType
import design.animus.kotlin.mp.schemas.openapi.utils.toClientName
import kotlinx.serialization.ExperimentalSerializationApi

data class OperationFuncResponse(
  val finalPackageName: String,
  val additionalTypes: List<TypeSpec>,
  val operationName: String,
  val operationFunc: FunSpec,
  val verb: String
)

@ExperimentalSerializationApi
data class OperationFuncArgs(
  val verb: String,
  val config: GeneratorConfig,
  val path: String,
  val pathDef: PathItem,
  val basePackageName: String,
  val urlParams: HandleUrlParameterResponse,
  val queryParams: QueryResponse,
  val reconciledParams: List<Parameter>,
  val operationFuncName: String,
  val parentClass: ClassName,
  val operation: Operation,
  val possibleResponses: PossibleResponses
)

@ExperimentalSerializationApi
suspend fun createOperationFuncFromPathItem(
  path: String,
  pathDef: PathItem,
  basePackageName: String,
  config: GeneratorConfig
): List<OperationFuncResponse> {
  val parentClassName = config.schemaName.toClientName(config)
  val parentClass = ClassName(basePackageName, parentClassName)
  return pathDef.schemaPatternProperties.filterIsInstance<PathItemWrapperMap.PathItem0Map>()
    .first()
    .map
    .map { (verb, operation) ->
      val (operationName, _, _) = operation.operationId!!.sanitizeForClassName()
      val operationFuncName = operationName.decapitalize()
      val urlParams = handleURLParameters(path, operation, config)
      val reconciledParams = operation.parameters?.reconcile(openAPI) ?: listOf()
      val queryParams = handleQueryParams(reconciledParams, config)
      val possibleResponse = determineReturnType(
        operationName,
        urlParams.packageName,
        operation.responses,
        openAPI,
        config
      )
      val args = OperationFuncArgs(
        verb = verb,
        config = config,
        path = path,
        pathDef = pathDef,
        urlParams = urlParams,
        basePackageName = basePackageName,
        reconciledParams = reconciledParams,
        queryParams = queryParams,
        operation = operation,
        operationFuncName = operationFuncName,
        parentClass = parentClass,
        possibleResponses = possibleResponse
      )
      val (operationFunc, additionalTypes) = when (val typedVerb = getHTTPVerb(verb)) {
        HTTPVerbs.GET -> createGetOperationFunc(args)
        else -> {
          logger.warn { "$typedVerb not implemented yet." }
          Pair(bootStrapOperationFunc(args).build(), listOf())
        }
      }
      OperationFuncResponse(urlParams.packageName, additionalTypes, operationFuncName, operationFunc, verb)
    }
}

@ExperimentalSerializationApi
suspend fun createOperationFuncFromPathItemAndWriteToFile(
  path: String,
  pathDef: PathItem,
  basePackageName: String,
  config: GeneratorConfig
) {
  val operationFuncs = createOperationFuncFromPathItem(path, pathDef, basePackageName, config)
  operationFuncs.forEach { (finalPackageName, additionalTypes, operationName, operationFunc, verb) ->
    val operationFile =
      fileSpecBootStrap(finalPackageName, operationName.capitalize())
        .addImport("io.ktor.client.request", verb, "parameter")
        .addFunction(operationFunc)
    additionalTypes.forEach { operationFile.addType(it) }
    operationFile
      .build()
      .writeTo(config.outputPath)
  }
}
