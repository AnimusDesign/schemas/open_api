package design.animus.kotlin.mp.schemas.openapi.responses

import com.squareup.kotlinpoet.*
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.toClassName
import design.animus.kotlin.mp.schemas.openapi.generated.OpenAPI
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Responses
import design.animus.kotlin.mp.schemas.openapi.generated.patternmaps.ResponsesWrapperMap
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfMediaTypeSchema
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfResponses
import design.animus.kotlin.mp.schemas.openapi.utils.toTypeName
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

suspend fun createParentClassName(operationName: String) = "${operationName}Responses"
suspend fun createNestedName(operationName: String, statusCode: String) = "${operationName}ResponseOf$statusCode"

@ExperimentalSerializationApi
suspend fun createResponseType(
  operationName: String,
  packageName: String,
  responses: Responses,
  openAPI: OpenAPI,
  config: GeneratorConfig
): TypeSpec {
  val parentResponseName = createParentClassName(operationName)
  val parentResponse = TypeSpec.classBuilder(parentResponseName)
    .addModifiers(KModifier.SEALED)
    .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
    .addAnnotation(Serializable::class)
  val responseDef = responses.schemaPatternProperties.filterIsInstance<ResponsesWrapperMap.Responses0Map>()
  val responseRef = responses.schemaPatternProperties.filterIsInstance<ResponsesWrapperMap.Responses1Map>()
  if (responseDef.isNotEmpty()) {
    val def = responseDef.first()
    def.map.forEach { (statusCode, definition) ->
      val paramType = when (definition) {
        is OneOfResponses.PossibleResponse -> {
          val response = definition.item.content?.schemaAdditionalProperties?.values?.toSet()
          if (response?.isNotEmpty() == true) {
            when (val schema = response.first().schema) {
              is OneOfMediaTypeSchema.PossibleSchema -> schema.item.toTypeName("", config)
              is OneOfMediaTypeSchema.PossibleReference -> schema.item.toTypeName(config)
              null -> JsonElement::class.toClassName()
            }
          } else {
            JsonElement::class.toClassName()
          }
        }
        is OneOfResponses.PossibleReference -> definition.item.toTypeName(config)
      }
      val responseType = TypeSpec.classBuilder(createNestedName(operationName, statusCode))
        .addModifiers(KModifier.DATA)
        .addAnnotation(Serializable::class)
        .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
        .superclass(ClassName(packageName, parentResponseName))
        .primaryConstructor(
          FunSpec.constructorBuilder()
            .addParameter("response", paramType)
            .build()
        )
        .addProperty(
          PropertySpec.builder("response", paramType)
            .initializer("response")
            .build()
        )
      parentResponse.addType(responseType.build())
    }
  }
  return parentResponse.build()
}
