package design.animus.kotlin.mp.schemas.openapi.operationfunc

import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.TypeSpec
import design.animus.kotlin.mp.schemas.openapi.operationfunc.common.addResponses
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
suspend fun createGetOperationFunc(
  args: OperationFuncArgs
): Pair<FunSpec, List<TypeSpec>> {
  val allParams = args.urlParams.params + args.queryParams.requiredParameters + args.queryParams.optionalParams
  val operationFunc = bootStrapOperationFunc(args)
    .addCommonClientInitStatement("get", args.urlParams.apiURL)
    .addParameters(allParams)
  args.queryParams.requiredCodeBlock.forEach {
    operationFunc.addCode(it)
  }
  args.queryParams.optionalCodeBlock.forEach {
    operationFunc.addCode(it)
  }
  val additionalTypes = operationFunc.addResponses(args.possibleResponses)
  return Pair(operationFunc.build(), additionalTypes)
}
