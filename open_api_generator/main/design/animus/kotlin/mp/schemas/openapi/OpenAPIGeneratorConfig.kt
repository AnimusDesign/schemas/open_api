package design.animus.kotlin.mp.schemas.openapi

import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig

data class OpenAPIConfig(
  val generatorConfig: GeneratorConfig,
  val createModel: Boolean = true,
  val createServer: Boolean = false,
  val createClient: Boolean = false
)

open class OpenAPIGeneratorConfig(
  var schemas: List<OpenAPIConfig>
)
