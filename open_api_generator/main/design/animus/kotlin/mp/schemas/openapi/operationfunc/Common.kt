package design.animus.kotlin.mp.schemas.openapi.operationfunc

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.KModifier
import com.squareup.kotlinpoet.ParameterSpec
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.sanitizeForClassName
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Operation
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfParameters
import design.animus.kotlin.mp.schemas.openapi.utils.toTypeName
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
suspend fun bootStrapOperationFunc(args: OperationFuncArgs) = FunSpec.builder(args.operationFuncName)
  .addModifiers(KModifier.SUSPEND)
  .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
  .receiver(args.parentClass)
  .addKdoc(
    """
        ${args.verb.toUpperCase()} -> ${args.path}
        ${args.operation.summary ?: ""}

        GENERATED
    """.trimIndent()
  )

@ExperimentalSerializationApi
suspend fun handleURLParameters(
  path: String,
  operation: Operation,
  config: GeneratorConfig
): HandleUrlParameterResponse {
  val urlParams = Regex("""\{\w+}""").find(path)
  return if (urlParams == null) {
    val pathAsPackageName = path.split("/").map { it.sanitizeForClassName().className }.joinToString(".")
    HandleUrlParameterResponse(
      packageName = "${config.packageBase}.$pathAsPackageName",
      apiURL = path,
      params = listOf()
    )
  } else {
    val extendedPath = urlParams.groups.filterNotNull().joinToString("/") { match ->
      val value = match.value
      val sanitizedValue = value.replace("{", "").replace("}", "")
      val paramName = "urlParam${sanitizedValue.capitalize()}"

      "\$$paramName"
    }
    val finalPath = path.split("/")
      .filter {
        !(it.contains("{") || it.contains("}"))
      }
      .map { it.sanitizeForClassName().className }
    val params = urlParams.groups.filterNotNull().map { match ->
      val value = match.value
      val sanitizedValue = value.replace("{", "").replace("}", "")
      val paramName = "urlParam${sanitizedValue.capitalize()}"
      val paramDef = operation.parameters?.filterIsInstance<OneOfParameters.PossibleParameter>()
        ?.find { it.item.name == sanitizedValue }!!
      ParameterSpec.builder(paramName, paramDef.item.schema!!.toTypeName(paramName, config))
        .addKdoc(paramDef.item.description ?: "")
        .build()
    }
    HandleUrlParameterResponse(
      packageName = "${config.packageBase}.${finalPath.joinToString(".")}",
      apiURL = "${finalPath.joinToString("/")}/$extendedPath",
      params = params
    )
  }
}

data class HandleUrlParameterResponse(val packageName: String, val apiURL: String, val params: List<ParameterSpec>)

suspend fun FunSpec.Builder.addCommonClientInitStatement(verb: String, path: String) =
  this
    .addStatement(
      """val rsp = this.client.$verb<String>(%L + "%L") {""",
      "this.apiURL.toString()",
      path
    )
