package design.animus.kotlin.mp.schemas.openapi.utils

import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.mutateNameIfEnabled

suspend fun String.toClientName(config: GeneratorConfig): String {
  val newName = mutateNameIfEnabled(this, config)
  return "${newName.className}Client"
}
