package design.animus.kotlin.mp.schemas.openapi.responses

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.containers.createObjectSerializerName
import design.animus.kotlin.mp.schemas.openapi.generated.OpenAPI
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Responses
import design.animus.kotlin.mp.schemas.openapi.generated.patternmaps.ResponsesWrapperMap
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerializationException
import kotlinx.serialization.Serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonDecoder
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject

fun buildNestedClassDeserializer(parent: String, name: String): String {
  return """
        val possible$name = try {
            Json.decodeFromJsonElement($parent.$name.serializer(), finalObj)
        } catch (e:Exception) {
            null
        }
  """.trimIndent()
}

@ExperimentalSerializationApi
suspend fun createResponseSerializer(
  operationName: String,
  packageName: String,
  responses: Responses,
  openAPI: OpenAPI,
  config: GeneratorConfig
): TypeSpec {
  val parentName = createParentClassName(operationName)
  val serializerName = createObjectSerializerName(parentName)
  val responseDef = responses.schemaPatternProperties.filterIsInstance<ResponsesWrapperMap.Responses0Map>()
  val responseRef = responses.schemaPatternProperties.filterIsInstance<ResponsesWrapperMap.Responses1Map>()
  val nested = if (responseDef.isNotEmpty()) {
    val def = responseDef.first()
    def.map.map { (statusCode, _) ->
      createNestedName(operationName, statusCode)
    }
  } else {
    listOf()
  }
  val typeSpec = TypeSpec.Companion.objectBuilder(serializerName)
    .addAnnotation(
      AnnotationSpec.builder(Serializer::class)
        .addMember("forClass = %L::class", parentName)
        .build()
    )
    .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
    .addSuperinterface(
      ClassName("kotlinx.serialization", "KSerializer")
        .parameterizedBy(ClassName(packageName, parentName))
    )
    .addFunction(
      FunSpec.builder("deserialize")
        .addModifiers(KModifier.OVERRIDE)
        .addParameter("decoder", Decoder::class)
        .addCode(
          """
                val input = decoder as? %T ?: throw %T(%S)
                val rawObj = input.decodeJsonElement()
                            as? %T ?:
                            throw %T(%S)
                val finalObj = %T(mapOf("response" to rawObj))
                val stub = %T::class
                ${nested.joinToString("\n") { buildNestedClassDeserializer(parentName, it) }}
                val result = ${nested.joinToString(" ?: ") { "possible$it" }}
                ?: throw Exception("Could not deserialize")
                return result
          """,
          JsonDecoder::class, SerializationException::class, "Expected Json Input",
          JsonElement::class, SerializationException::class, "Didn't find json object",
          JsonObject::class, Json::class
        )
        .returns(ClassName(packageName, parentName))
        .build()
    )
    .addFunction(
      FunSpec.builder("serialize")
        .addModifiers(KModifier.OVERRIDE)
        .addParameter("encoder", Encoder::class)
        .addParameter("value", ClassName(packageName, parentName))
        .addCode(
          """
                val data = when (value) {
                ${
          nested.joinToString("\n") {
            "     is $parentName.$it -> encoder.encodeSerializableValue($parentName.$it.serializer(), value)"
          }
          }
                }

          """.trimIndent()
        )
        .build()
    )
  return typeSpec.build()
}
