package design.animus.kotlin.mp.schemas.openapi.responses

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.TypeSpec
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.openapi.generated.OpenAPI
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Responses
import design.animus.kotlin.mp.schemas.openapi.logger
import kotlinx.serialization.ExperimentalSerializationApi

sealed class PossibleResponses {
  object Blank : PossibleResponses()
  data class SerializableResponse(val responseType: TypeSpec, val responseSerializer: TypeSpec, val responseRef: ClassName) : PossibleResponses()
}

@ExperimentalSerializationApi
suspend fun determineReturnType(
  operationName: String,
  packageName: String,
  responses: Responses,
  openAPI: OpenAPI,
  config: GeneratorConfig
): PossibleResponses {
  return if (responses.default != null && responses.schemaPatternProperties.isEmpty()) {
    logger.debug { "Default response but not additional responses, set to Blank" }
    PossibleResponses.Blank
  } else {
    val responseType = createResponseType(operationName, packageName, responses, openAPI, config)
    val responseSerializer = createResponseSerializer(operationName, packageName, responses, openAPI, config)
    val responseRef = ClassName(packageName, createParentClassName(operationName))
    PossibleResponses.SerializableResponse(responseType, responseSerializer, responseRef)
  }
}
