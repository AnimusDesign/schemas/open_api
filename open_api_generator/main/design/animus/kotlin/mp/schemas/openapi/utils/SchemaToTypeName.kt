package design.animus.kotlin.mp.schemas.openapi.utils

import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.TypeName
import design.animus.kotlin.mp.schemas.json.generator.*
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Schema
import design.animus.kotlin.mp.schemas.openapi.generated.enums.EnumSchemaType
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfMediaTypeSchema
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfParameterSchema
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfSchemaItems
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject

@ExperimentalSerializationApi
suspend fun Schema.toTypeName(name: String, config: GeneratorConfig): TypeName {
  val rsp = if (this.enum != null && this.enum!!.isNotEmpty()) {
    createEnum(name, config)
  } else {
    when (this.type) {
      EnumSchemaType.array -> {
        val nestedType = when (val items = this.items) {
          is OneOfSchemaItems.PossibleSchema -> items.item.toTypeName(name, config)
          is OneOfSchemaItems.PossibleReference -> items.item.toTypeName(config)
          null -> JsonElement::class.toClassName()
        }
        List::class.toClassName().parameterizedBy(nestedType)
      }
      EnumSchemaType.boolean -> Boolean::class.toClassName()
      EnumSchemaType.integer -> Int::class.toClassName()
      EnumSchemaType.number -> Number::class.toClassName()
      EnumSchemaType.`object` -> JsonObject::class.toClassName()
      EnumSchemaType.string -> String::class.toClassName()
      null -> String::class.toClassName()
    }
  }
  return rsp
}

@ExperimentalSerializationApi
suspend fun OneOfParameterSchema.toTypeName(name: String, config: GeneratorConfig): TypeName {
  return when (this) {
    is OneOfParameterSchema.PossibleSchema -> this.item.toTypeName(name, config)
    is OneOfParameterSchema.PossibleReference -> this.item.toTypeName(config)
  }
}

@ExperimentalSerializationApi
suspend fun OneOfMediaTypeSchema.toTypeName(name: String, config: GeneratorConfig): TypeName {
  return when (this) {
    is OneOfMediaTypeSchema.PossibleSchema -> this.item.toTypeName(name, config)
    is OneOfMediaTypeSchema.PossibleReference -> this.item.toTypeName(config)
  }
}
