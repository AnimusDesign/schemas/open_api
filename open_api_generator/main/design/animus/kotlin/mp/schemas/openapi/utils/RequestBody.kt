package design.animus.kotlin.mp.schemas.openapi.utils

import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Reference
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.RequestBody
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfComponentsRequestBodies
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfOperationRequestBody
import design.animus.kotlin.mp.schemas.openapi.openAPI
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
/**
 * todo Add support for Reference RequestBody
 */
suspend fun OneOfOperationRequestBody.toRequestBody(): RequestBody {
  return when (this) {
    is OneOfOperationRequestBody.PossibleRequestBody -> this.item
    is OneOfOperationRequestBody.PossibleReference -> this.item.toRequestBody()
  }
}

@ExperimentalSerializationApi
suspend fun Reference.toRequestBody(): RequestBody {
  val ref = this.schemaPatternProperties.map["\$ref"]
    ?: error("Was expecting a reference but none found.")
  val item = openAPI.components?.requestBodies?.schemaPatternProperties?.map!![ref.last()]
    ?: error("Could not find request body by reference: $ref")
  return when (item) {
    is OneOfComponentsRequestBodies.PossibleReference -> item.item.toRequestBody()
    is OneOfComponentsRequestBodies.PossibleRequestBody -> item.item
  }
}

@ExperimentalSerializationApi
suspend fun RequestBody.containsForm(): Boolean {
  return this.content.schemaAdditionalProperties.keys.any { it.contains("form") }
}

@ExperimentalSerializationApi
suspend fun RequestBody.hasMultipleBodies(config: GeneratorConfig): Boolean {
  return this.content.schemaAdditionalProperties.values.mapNotNull {
    it.schema?.toTypeName("", config)
  }.toSet().size > 1
}
