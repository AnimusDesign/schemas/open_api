package design.animus.kotlin.mp.schemas.openapi.params

import com.squareup.kotlinpoet.ParameterSpec
import com.squareup.kotlinpoet.PropertySpec
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Parameter
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfParameterSchema
import design.animus.kotlin.mp.schemas.openapi.utils.toTypeName
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
suspend fun createFunctionParameterForSchema(param: Parameter, config: GeneratorConfig): ParameterSpec {
  val paramSchema = param.schema as? OneOfParameterSchema.PossibleSchema
  val paramSchemaType = paramSchema?.item?.toTypeName(param.name, config)
    ?: error("Schema was null.")
  return ParameterSpec.builder(param.name, paramSchemaType.copy(nullable = !(param.required ?: false)))
    .addKdoc(paramSchema.item.description ?: "")
    .build()
}

@ExperimentalSerializationApi
suspend fun createPropertyForSchema(param: Parameter, config: GeneratorConfig, initialized: Boolean = true): PropertySpec {
  val paramSchema = param.schema as? OneOfParameterSchema.PossibleSchema
  val paramSchemaType = paramSchema?.item?.toTypeName(param.name, config)
    ?: error("Schema was null.")
  return PropertySpec.builder(param.name, paramSchemaType.copy(nullable = !(param.required ?: false)))
    .addKdoc(paramSchema.item.description ?: "")
    .initializer(param.name)
    .build()
}
