package design.animus.kotlin.mp.schemas.openapi.client

import com.squareup.kotlinpoet.*
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.fileSpecBootStrap
import design.animus.kotlin.mp.schemas.openapi.utils.toClientName
import io.ktor.client.*
import io.ktor.http.*

suspend fun createParent(basePackageName: String, parentName: String, config: GeneratorConfig): TypeSpec {
  val parent = TypeSpec.classBuilder(parentName)
    .addAnnotation(ClassName("kotlinx.serialization", "ExperimentalSerializationApi"))
    .primaryConstructor(
      FunSpec.constructorBuilder()
        .addParameter("apiURL", Url::class)
        .build()
    )
    .addProperty(
      PropertySpec.builder("client", HttpClient::class)
        .addModifiers(KModifier.LATEINIT)
        .mutable(true)
        .build()
    )
    .addProperty(
      PropertySpec.builder("apiURL", Url::class)
        .initializer("apiURL")
        .build()
    )
    .addInitializerBlock(
      CodeBlock.of(
        """
                    client = %T() {
                                    headersOf(
                                        Pair("Content-Type", listOf("application/json")),
                                        Pair("Accept", listOf("application/json"))
                                    )
                                }
        """.trimIndent(),
        HttpClient::class,
      )
    )
  return parent.build()
}

suspend fun createAndWriteParent(config: GeneratorConfig): TypeSpec {
  val parentName = config.schemaName.toClientName(config)
  val file = fileSpecBootStrap(config.packageBase, parentName)
    .addImport("io.ktor.http", "headersOf")
  val parentClass = createParent(config.packageBase, parentName, config)
  file.addType(parentClass).build().writeTo(config.outputPath)
  return parentClass
}
