package design.animus.kotlin.mp.schemas.openapi.utils

import design.animus.kotlin.mp.schemas.openapi.generated.OpenAPI
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.SecurityScheme
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfComponentsSecuritySchemes
import design.animus.kotlin.mp.schemas.openapi.logger

suspend fun getSecuritySchemes(schema: OpenAPI) {
  val securityMap = schema.components?.securitySchemes?.schemaPatternProperties?.map
    ?: mapOf()
  securityMap
    .filter { it.value is OneOfComponentsSecuritySchemes.PossibleSecurityScheme }
    .map { (k, v) -> Pair(k, v as OneOfComponentsSecuritySchemes.PossibleSecurityScheme) }
    .forEach { (t, u) ->
      when (u.item) {
        is SecurityScheme.PossibleAPIKeySecurityScheme -> TODO()
        else -> {
          logger.warn { "Only api key is supported as this time" }
        }
      }
    }
}
