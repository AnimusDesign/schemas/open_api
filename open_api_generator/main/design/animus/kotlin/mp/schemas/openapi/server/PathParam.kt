package design.animus.kotlin.mp.schemas.openapi.server

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import design.animus.functional.datatypes.either.catchEitherAsync
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.json.generator.toClassName
import design.animus.kotlin.mp.schemas.openapi.generated.definitions.Parameter
import design.animus.kotlin.mp.schemas.openapi.params.createFunctionParameterForSchema
import design.animus.kotlin.mp.schemas.openapi.params.createPropertyForSchema
import design.animus.kotlin.mp.schemas.openapi.utils.toType
import design.animus.kotlin.mp.schemas.openapi.utils.toTypeName
import kotlinx.coroutines.CoroutineScope

suspend fun createPathParameterAsync(
  pathParams: List<Parameter>,
  operationName: String,
  packageName: String,
  config: GeneratorConfig,
  ctx: CoroutineScope
) = ctx.catchEitherAsync {
  val dataClassName = "${operationName}PathParams"
  val dataClassType = ClassName(packageName, dataClassName)
  val pathParamList = pathParams.map {
    val type = it.schema?.toType(it.name, config) ?: String::class
    """ParamPosition( "${it.name}", ${it.required ?: false},  ${type.qualifiedName}::class )"""
  }
  val pathData = TypeSpec.classBuilder(dataClassName)
    .addModifiers(KModifier.DATA)
    .addSuperinterface(IOpenAPIPathParams::class)
    .primaryConstructor(
      FunSpec.constructorBuilder()
        .addParameters(
          pathParams.map { createFunctionParameterForSchema(it, config) }
        )
        .build()
    )
    .addProperties(
      pathParams.map { createPropertyForSchema(it, config) }
    )
    .build()
  val pathParamProp = PropertySpec.builder(
    "paramByPosition",
    List::class.parameterizedBy(
      ParamPosition::class
    )
  )
    .addModifiers(KModifier.OVERRIDE)
    .initializer(
      "listOf(%L)",
      pathParamList.joinToString(",")
    )
    .build()
  val createPathParamFunc = FunSpec.builder("createPathParams")
    .addModifiers(KModifier.OVERRIDE)
    .addParameter("items", Any::class.toClassName().copy(nullable = true), KModifier.VARARG)
    .addStatement(
      createPathParamFuncBody(dataClassName, pathParams, config),
      pathParams.map { it.schema?.toTypeName(it.name, config) }.toTypedArray()
    )
    .returns(dataClassType)
    .build()
  Triple(pathData, pathParamProp, createPathParamFunc)
}

suspend fun createPathParamFuncBody(dataClass: String, pathParams: List<Parameter>, config: GeneratorConfig): String {
  val statement = pathParams.mapIndexed { idx, it ->
    val type = it.schema?.toTypeName(it.name, config)
    val nullable = if (it.required ?: false) "" else "?"
    "castPathParam(items[$idx] , paramByPosition[$idx]) as $type$nullable"
  }
  return """
        return $dataClass(
            ${
  statement.joinToString(",")
  }
        )
  """.trimIndent()
}
