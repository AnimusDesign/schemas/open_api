package design.animus.kotlin.mp.schemas.openapi.server

sealed class HTTPVerb {
  object GET : HTTPVerb()
  object POST : HTTPVerb()
  object PUT : HTTPVerb()
  object DELETE : HTTPVerb()
  object OPTIONS : HTTPVerb()

  companion object {
    fun fromString(value: String): HTTPVerb {
      return when (val lowerValue = value.toLowerCase()) {
        "get" -> GET
        "delete" -> DELETE
        "post" -> POST
        "put" -> PUT
        "options" -> OPTIONS
        else -> throw Exception("Verb did not match known verb")
      }
    }
  }
}
