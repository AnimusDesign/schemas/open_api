package design.animus.kotlin.mp.schemas.openapi.server

import design.animus.functional.datatypes.option.Option
import kotlin.js.ExperimentalJsExport
import kotlin.reflect.KClass

typealias OpenAPIPath = String
typealias OpenAPIOperation = String

interface IOpenAPIQueryParams

data class ParamPosition(val name: String, val required: Boolean, val type: KClass<*>)
interface IOpenAPIPathParams

@ExperimentalJsExport
interface IOpenAPIRequest<Body : Any> {
  val body: Option<Body>
  val queryParams: Option<IOpenAPIQueryParams>
  val urlParams: Option<IOpenAPIPathParams>
}

@ExperimentalJsExport
interface IOpenAPI {
  val operationName: OpenAPIOperation
  val path: OpenAPIPath
  val verb: HTTPVerb
}

interface IOpenAPIWithPathParameters<out PP : IOpenAPIPathParams> {
  val paramByPosition: List<ParamPosition>
  fun createPathParams(vararg items: Any?): PP

  fun castPathParam(value: Any?, param: ParamPosition): Any? {
    if (value == null && !param.required) {
      return null
    }
    return when (param.type) {
      Boolean::class -> value.toString().toBoolean()
      Int::class -> value.toString().toInt()
      String::class -> value.toString()
      else -> throw error("Encountered an ${param.name} unknown type: ${param.type}")
    }
  }
}
