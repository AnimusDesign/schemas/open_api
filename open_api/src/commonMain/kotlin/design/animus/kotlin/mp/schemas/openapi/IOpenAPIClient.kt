package design.animus.kotlin.mp.schemas.openapi

import io.ktor.client.*
import io.ktor.http.*

interface IOpenAPIClient {
  abstract val apiURL: Url
  abstract val client: HttpClient
}
