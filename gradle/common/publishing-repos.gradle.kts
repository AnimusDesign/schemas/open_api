import java.net.URI

buildscript {
    apply("$rootDir/versions.gradle.kts")
}

apply(plugin = "maven-publish")

configure<PublishingExtension> {
    repositories {
        maven {
            url = URI(System.getenv("MavenURL") ?: "")
            credentials {
                username = System.getenv("MavenUser") ?: ""
                password = System.getenv("MavenPassword") ?: ""
            }
        }
    }

}
