import java.net.URI

buildscript {
    repositories {
        mavenLocal()
        maven { url = uri("https://gitlab.com/api/v4/groups/2057351/-/packages/maven") }
        maven { url = uri("https://gitlab.com/api/v4/groups/9931920/-/packages/maven") }
        maven { url = uri("https://animusdesign-repository.appspot.com") }
        maven { url = uri("https://plugins.gradle.org/m2/") }
        maven { url = uri("https://dl.bintray.com/kotlin/kotlin-eap") }
        maven { url = uri("https://kotlin.bintray.com/kotlinx") }
        jcenter()
        mavenCentral()
        gradlePluginPortal()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.30")
        classpath("org.jlleitschuh.gradle:ktlint-gradle:9.4.0")
        classpath("com.github.jengelman.gradle.plugins:shadow:6.0.0")
        classpath("org.jetbrains.kotlin:kotlin-serialization:1.4.30")

    }
}

allprojects {
    repositories {
        mavenLocal()
        jcenter()
        mavenCentral()
        maven { url = uri("https://gitlab.com/api/v4/groups/2057351/-/packages/maven") }
        maven { url = uri("https://gitlab.com/api/v4/groups/9931920/-/packages/maven") }
        maven { url = uri("https://animusdesign-repository.appspot.com") }
        maven { url = uri("https://dl.bintray.com/kotlin/kotlin-eap") }
        maven { url = uri("https://oss.jfrog.org/oss-release-local") }
        maven { url = uri("https://kotlin.bintray.com/kotlinx/") }
        maven { url = uri("https://jitpack.io") }
    }
}

subprojects {
    if (project.path.endsWith("open_api_generator") || project.path.endsWith("petstore") || project.path.endsWith("ktor") || project.path.endsWith("ktor-server")) {
        apply(plugin = "org.jetbrains.kotlin.jvm")
    } else if (project.path.endsWith("open_api") ) {
        apply(plugin="org.jetbrains.kotlin.multiplatform")
    }
    apply(plugin="org.jetbrains.kotlin.plugin.serialization")
    apply(plugin = "org.jlleitschuh.gradle.ktlint")
    apply(plugin="maven-publish")
    configure<org.jlleitschuh.gradle.ktlint.KtlintExtension> {
        debug.set(true)
        verbose.set(true)
        android.set(false)
        enableExperimentalRules.set(true)
        additionalEditorconfigFile.set(file("$rootDir/.editorconfig"))
        this.filter(
            Action {
                include("**/kotlin/**", "**/main/**")
                exclude("**/generated/**", "**/petstore/**")
            }
        )
    }
    val version: String by project
    val baseGroup = "design.animus.kotlin.mp.schemas"
    group = baseGroup
    this.version = version
    configure<PublishingExtension> {
        repositories {
            maven {
                url = URI("https://gitlab.com/api/v4/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven/")
                credentials(HttpHeaderCredentials::class.java) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }
}
