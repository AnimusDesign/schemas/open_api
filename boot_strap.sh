#!/usr/bin/env bash
rm -fv open_api/src/commonMain/resources/openapi-3.0.schema.json
wget https://raw.githubusercontent.com/OAI/OpenAPI-Specification/3.1.0/schemas/v3.0/schema.json \
  -O open_api/src/commonMain/resources/openapi-3.0.schema.json
