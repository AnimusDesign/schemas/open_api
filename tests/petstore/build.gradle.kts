import design.animus.kotlin.contract.Versions.Dependencies
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import java.io.File
import design.animus.kotlin.mp.schemas.openapi.OpenAPIConfig
plugins {
  id("design.animus.kotlin.mp.schemas.open_api.generator")
}

kotlin {
  sourceSets {
    main {
      kotlin.setSrcDirs(mutableListOf("main", "generated"))
      dependencies {
          implementation(project(":open_api:"))

          api("design.animus.kotlin.mp.schemas:json_parser:${Dependencies.mpJSONSchema}")
        api("design.animus.kotlin.mp.schemas:json_generator:${Dependencies.mpJSONSchema}")
        api("com.benasher44:uuid:${Dependencies.mpUUID}")
        api("org.jetbrains.kotlinx:kotlinx-datetime:${Dependencies.mpDate}")
        implementation("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dependencies.kotlin}")
        implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
        implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:${Dependencies.serialization}")
        implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
        implementation("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
        implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
        implementation("io.github.microutils:kotlin-logging-jvm:${Dependencies.kotlinLogging}")
        implementation("ch.qos.logback:logback-core:${Dependencies.logback}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
        implementation("design.animus.kotlin.mp:datatypes-jvm:${Dependencies.MPDataTypes}")
      }
    }
    test {
      dependencies {
        implementation(project(":open_api:"))
        implementation(kotlin("stdlib-jdk8"))
        implementation(kotlin("test-junit"))
        implementation(kotlin("test"))
      }
    }
  }
}

task("copyTestResources", Copy::class) {
  from("$projectDir/test/resources")
  into("$buildDir/classes/kotlin/test")
}
task("copyMainResources", Copy::class) {
  from("$projectDir/main/resources")
  into("$buildDir/classes/kotlin/main")
}
tasks.named("build") {
  dependsOn("processTestResources", "copyTestResources", "copyMainResources")
}

tasks.named("test") {
  dependsOn("processTestResources", "copyTestResources")
}
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions.jvmTarget = "11"
}

publishing {
  publications {
    create<MavenPublication>("maven") {
      this.groupId = group.toString()
      this.version = version
      artifactId = "kotlin-mp-contract-asyncapi"
      from(components["java"])
    }
  }
}

tasks.withType<Test>().configureEach {
  useJUnit()
  reports.junitXml.isEnabled = true
}

OpenAPIGeneratorConfig {
    schemas = listOf(
        OpenAPIConfig(
            GeneratorConfig(
                packageBase = "design.animus.kotlin.mp.schemas.openapi.tests.petstore",
                outputPath = File("$projectDir/generated"),
                schemaFile = File("$projectDir/main/resources/petstore.api.json"),
                schemaName = "PetStore",
                createBaseObject = false
            ),
            createServer = false,
            createModel = true,
            createClient = false
        )
    )
}
