package design.animus.kotlin.mp.schemas.openapi.test

import java.io.File

interface IOpenAPITestBase {

  fun loadTestSchema(fileName: String): String {
    val clazz = IOpenAPITestBase::class.java.classLoader.getResource(fileName)
    val file = File(clazz.path)
    return file.readText()
  }
}
