package design.animus.kotlin.mp.schemas.openapi.test

import design.animus.kotlin.mp.schemas.openapi.generated.OpenAPI
import design.animus.kotlin.mp.schemas.openapi.generated.patternmaps.PathItemWrapperMap
import design.animus.kotlin.mp.schemas.openapi.generated.patternmaps.PathsWrapperMap
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfMediaTypeSchema
import design.animus.kotlin.mp.schemas.openapi.generated.unions.oneof.OneOfOperationRequestBody
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import org.junit.Before
import kotlin.test.Test
import kotlin.test.assertTrue

@ExperimentalSerializationApi
class TestPaths : IOpenAPITestBase {
  private lateinit var schema: OpenAPI

  @Before
  fun loadSchema() {
    val raw = loadTestSchema("petstore.api.json")
    schema = Json.decodeFromString(OpenAPI.serializer(), raw)
  }

  @Test
  fun testPaths() {
    assertTrue("There is only one type of pattern property for paths") {
      schema.paths.schemaPatternProperties.size == 1
    }
    assertTrue("Pattern property is Paths0Map") {
      schema.paths.schemaPatternProperties.first() is PathsWrapperMap.Paths0Map
    }

    val paths = schema.paths.schemaPatternProperties.first() as PathsWrapperMap.Paths0Map

    assertTrue("Pattern map contains all expected paths.") {
      paths.map.keys.containsAll(
        listOf(
          "/pet",
          "/pet/findByStatus",
          "/pet/findByTags",
          "/pet/{petId}",
          "/pet/{petId}/uploadImage",
          "/store/inventory",
          "/store/order",
          "/store/order/{orderId}",
          "/user",
          "/user/createWithArray",
          "/user/createWithList",
          "/user/login",
          "/user/logout",
          "/user/{username}"
        )
      )
    }

    val petPath = paths.map["/pet"] ?: error("/pet not found in paths")
    assertTrue("petPath has two verbs") { petPath.schemaPatternProperties.size == 1 }
    assertTrue("There is only one type of pattern property for paths") {
      petPath.schemaPatternProperties.size == 1
    }
    assertTrue("Pattern property is Paths0Map") {
      petPath.schemaPatternProperties.first() is PathItemWrapperMap.PathItem0Map
    }
    val petPathDef = petPath.schemaPatternProperties.first() as PathItemWrapperMap.PathItem0Map
    assertTrue("/pet contains a put & post") {
      petPathDef.map.keys.containsAll(listOf("put", "post"))
    }
    val petPathPut = petPathDef.map["put"] ?: error("/pet didn't find put")
    assertTrue("/pet put has operation id") { petPathPut.operationId == "updatePet" }
    assertTrue("RequestBody is PossibleRequestReady") {
      petPathPut.requestBody is OneOfOperationRequestBody.PossibleRequestBody
    }
    val petPathRequest = petPathPut.requestBody as OneOfOperationRequestBody.PossibleRequestBody
    assertTrue("/pet request body accepts json & xml") {
      petPathRequest.item.content.schemaAdditionalProperties.keys.containsAll(
        listOf(
          "application/json",
          "application/xml"
        )
      )
    }
    val petPathReqJson = petPathRequest.item.content.schemaAdditionalProperties["application/json"]
      ?: error("Could not load json content type")
    println(petPathReqJson)
    assertTrue("json schema is a reference") {
      petPathReqJson.schema is OneOfMediaTypeSchema.PossibleReference
    }
    assertTrue {
      (petPathReqJson.schema as OneOfMediaTypeSchema.PossibleReference).item.schemaPatternProperties.map["\$ref"] == "#/components/schemas/Pet"
    }
  }
}
