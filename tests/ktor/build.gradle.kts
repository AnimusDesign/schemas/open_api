import design.animus.kotlin.contract.Versions.Dependencies
import design.animus.kotlin.mp.schemas.json.generator.GeneratorConfig
import design.animus.kotlin.mp.schemas.openapi.OpenAPIConfig

plugins {
  application
  id("design.animus.kotlin.mp.schemas.open_api.generator")
}

application {
  mainClass.set("io.ktor.server.netty.EngineMain")
}

kotlin {
  sourceSets {
    main {
      kotlin.setSrcDirs(mutableListOf("main", "generated"))
      dependencies {
        implementation("io.ktor:ktor-server-core:${Dependencies.ktor}")
        implementation("io.ktor:ktor-server-host-common:${Dependencies.ktor}")
        implementation("io.ktor:ktor-metrics:${Dependencies.ktor}")
        implementation("io.ktor:ktor-metrics-micrometer:${Dependencies.ktor}")
        implementation("io.micrometer:micrometer-registry-prometheus:${Dependencies.micrometerPrometheus}")
        implementation("io.ktor:ktor-serialization:${Dependencies.ktor}")
        implementation("io.ktor:ktor-server-netty:${Dependencies.ktor}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
        implementation(project(":open_api:"))
        implementation(project(":extensions:ktor-server:"))
      }
    }
    test {
      kotlin.setSrcDirs(mutableListOf("test"))
      dependencies {
        implementation("io.ktor:ktor-server-tests:${Dependencies.ktor}")
      }
    }
  }
}

task("copyTestResources", Copy::class) {
  from("$projectDir/test/resources")
  into("$buildDir/classes/kotlin/test")
}
task("copyMainResources", Copy::class) {
  from("$projectDir/main/resources")
  into("$buildDir/classes/kotlin/main")
}
tasks.named("assemble") {
  dependsOn("processTestResources", "copyTestResources", "copyMainResources")
}

tasks.named("test") {
  dependsOn("processTestResources", "copyTestResources")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions.jvmTarget = "11"
  kotlinOptions.apiVersion = "1.4"
}

OpenAPIGeneratorConfig {
  schemas = listOf(
    OpenAPIConfig(
      GeneratorConfig(
        packageBase = "design.animus.kotlin.mp.schemas.openapi.tests.petstore",
        outputPath = File("$projectDir/generated"),
        schemaFile = File("$projectDir/main/resources/petstore.api.json"),
        schemaName = "PetStore",
        createBaseObject = false
      ),
      createServer = true,
      createModel = true,
      createClient = false
    )
  )
}

tasks.withType<Test>().configureEach {
    useJUnit()
    reports.junitXml.isEnabled = true
}
