package com.example

import io.ktor.http.*
import io.ktor.server.testing.*
import kotlin.test.Test
import kotlin.test.assertEquals

class ApplicationTest {
    @Test
    fun testRoot() {
        withTestApplication({ module(testing = true) }) {
            handleRequest(HttpMethod.Get, "/pet/1").apply {
                assertEquals(HttpStatusCode.OK, response.status())
                val content = this.response.content ?: ""
                assert(content == "1")
            }
        }
    }
}
