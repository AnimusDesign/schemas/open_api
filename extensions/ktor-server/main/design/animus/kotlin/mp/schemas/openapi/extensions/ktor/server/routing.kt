package design.animus.kotlin.mp.schemas.openapi.extensions.ktor.server

import design.animus.kotlin.mp.schemas.openapi.server.*
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.routing.*
import io.ktor.util.pipeline.*

fun <O, PP> Route.fromOpenAPI(
  operation: O,
  body: suspend (PP, PipelineContext<Unit, ApplicationCall>) -> Unit
): Route
    where O : IOpenAPI,
           O : IOpenAPIWithPathParameters<PP>,
           PP : IOpenAPIPathParams {
  val method = when (operation.verb) {
    HTTPVerb.DELETE -> HttpMethod.Delete
    HTTPVerb.GET -> HttpMethod.Get
    HTTPVerb.OPTIONS -> HttpMethod.Options
    HTTPVerb.POST -> HttpMethod.Post
    HTTPVerb.PUT -> HttpMethod.Put
  }
  return route(operation.path, method) {
    handle {
      val pathParams = operation.createPathParams(*call.parameters.asVarArg(operation.paramByPosition))
      body(pathParams, this)
    }
  }
}

suspend fun Parameters.asVarArg(paramByPosition: List<ParamPosition>): Array<Any?> {
  val args = mutableListOf<Any?>()
  var idx = 0
  this.forEach { key, item ->
    val expectedParam = paramByPosition[idx]
    val value = item.firstOrNull()
    assert(key == expectedParam.name)
    if (expectedParam.required) {
      assert(value != null)
      args.add(value!!)
    } else {
      args.add(value)
    }
    idx += 1
  }
  return args.map { it as Any? }.toTypedArray()
}
