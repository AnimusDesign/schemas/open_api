import design.animus.kotlin.contract.Versions.Dependencies

kotlin {
  sourceSets {
    main {
      kotlin.setSrcDirs(mutableListOf("main", "generated"))
      dependencies {
        api(project(":open_api:"))
        implementation("io.ktor:ktor-server-core:${Dependencies.ktor}")
        implementation("io.ktor:ktor-server-host-common:${Dependencies.ktor}")
        implementation("io.ktor:ktor-metrics:${Dependencies.ktor}")
        implementation("io.ktor:ktor-metrics-micrometer:${Dependencies.ktor}")
        implementation("io.micrometer:micrometer-registry-prometheus:${Dependencies.micrometerPrometheus}")
        implementation("io.ktor:ktor-serialization:${Dependencies.ktor}")
        implementation("io.ktor:ktor-server-netty:${Dependencies.ktor}")
        implementation("ch.qos.logback:logback-classic:${Dependencies.logback}")
      }
    }
    test {
      kotlin.setSrcDirs(mutableListOf("test"))
      dependencies {
        implementation("io.ktor:ktor-server-tests:${Dependencies.ktor}")
      }
    }
  }
}

task("copyTestResources", Copy::class) {
  from("$projectDir/test/resources")
  into("$buildDir/classes/kotlin/test")
}
task("copyMainResources", Copy::class) {
  from("$projectDir/main/resources")
  into("$buildDir/classes/kotlin/main")
}
tasks.named("assemble") {
  dependsOn("processTestResources", "copyTestResources", "copyMainResources")
}

tasks.named("test") {
  dependsOn("processTestResources", "copyTestResources")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions.jvmTarget = "11"
  kotlinOptions.apiVersion = "1.4"
}
