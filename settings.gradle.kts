import java.net.URI

enableFeaturePreview("GRADLE_METADATA")
rootProject.name = "open_api"

pluginManagement {
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id.startsWith("org.jetbrains.dokka")) {
                // Update Version Build Source if being changed.
                useVersion("1.4.20")
            }
            if (requested.id.id.startsWith("org.jetbrains.kotlin.") || requested.id.id.startsWith("org.jetbrains.kotlin.plugin.serialization")) {
                // Update Version Build Source if being changed.
                useVersion("1.4.32")
            }
            if (
                requested.id.id.startsWith("design.animus.kotlin.mp.schemas.json.generator")
            ) {
                useVersion("0.0.2")
            }
            if (
                requested.id.id.startsWith("design.animus.kotlin.mp.schemas.open_api.generator")
            ) {
                useVersion("0.0.1-SNAPSHOT")
            }
            if (
                requested.id.id.startsWith("org.gradle.kotlin.kotlin-dsl")
            ) {
                useVersion("2.0.0")
            }
        }
    }
    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
        gradlePluginPortal()
        maven { url = uri("https://dl.bintray.com/kotlin/kotlinx") }
        maven { url = uri("https://kotlin.bintray.com/kotlin-dev") }
        maven { url = uri("https://gitlab.com/api/v4/groups/2057351/-/packages/maven") }
        maven { url = uri("https://gitlab.com/api/v4/groups/9931920/-/packages/maven") }
        maven { url = uri("https://animusdesign-repository.appspot.com") }
    }
}

include(":open_api:")
include(":open_api_generator:")

include(":extensions:ktor-server:")
